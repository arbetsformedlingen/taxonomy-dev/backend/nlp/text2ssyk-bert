# Based on https://medium.com/@albertazzir/blazing-fast-python-docker-builds-with-poetry-a78a66f5aed0

#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
#;;;
#;;; B U I L D E R   I M A G E
#;;;
#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FROM docker.io/library/python:3.13.2-bookworm as builder

RUN pip install poetry==1.8

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /app

COPY text2ssyk_bert /app/text2ssyk_bert
COPY pyproject.toml poetry.lock README.md /app

ENV TEXT2SSYK_CONFIG_PATH=/app/data/config0 \
    PYTHONPATH=/app

RUN chmod g+r /app/* &&\
    python3 -m keyring --disable &&\
    poetry install

#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
#;;;
#;;; R U N T I M E   I M A G E
#;;;
#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FROM docker.io/library/python:3.13.2-slim-bookworm as runtime

WORKDIR /app

ENV VIRTUAL_ENV=/app/.venv \
    PATH="/app/.venv/bin:$PATH" \
    PYTHONPATH=/app

COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}
COPY main_config /app/
COPY data/config0/*.json /app/data/config0/
COPY data/config0/tokenizer /app/data/config0/tokenizer/
COPY text2ssyk_bert ./text2ssyk_bert
COPY data/config0/trained_bert/config.json data/config0/trained_bert/rng_state.pth  /app/data/config0/trained_bert/

ENTRYPOINT ["python", "/app/text2ssyk_bert/main.py"]