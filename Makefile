.PHONY: build-docker-image run-docker-example

docker_exec = podman

test:
	poetry run python3 -m unittest discover

specific-test:
	poetry run python3 -m unittest tests.test_main

run-stdin:
	poetry run python3 text2ssyk_bert/main.py

run-stdin-with-cache:
	poetry run python3 text2ssyk_bert/main.py --cache-file /tmp/democache.db --timeout-seconds 5


run-example: $(dev_models)
	echo '[{"id": "a", "originalJobPosting": {"title": "Pizzamästare", "description": "Det är meriterande om du kan baka pizza." }}, {"id": "b", "originalJobPosting": { "description": "Torka damm från datorskärmar och skrivbord.", "title": "Städare"}}]' | make run-stdin

make-example-data:
	cat ~/Downloads/text2ssyk/text_2_ssyk.out_20k-ads | head -n 1 > sampledata.jsonl

run-example-batch:
	cat ~/Downloads/text2ssyk/text_2_ssyk.out_20k-ads | make run-stdin-with-cache > out.jsonl

requirements.txt: pyproject.toml
	poetry export -f requirements.txt --without-hashes --output requirements.txt

build-docker-image: requirements.txt
	$(docker_exec) build -t text2ssyk .

run-docker-example:
	echo '{"id": "x", "originalJobPosting": { "description":"baka pizza" }}' | $(docker_exec) run -i text2ssyk
