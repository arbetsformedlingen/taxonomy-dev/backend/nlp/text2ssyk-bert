# text2ssyk-bert

This project implements a classifier that classifies job ads to SSYK4 occupation classification codes. For a related approach, see [text-2-ssyk][102].

On a laptop, it classifies about 10000 job ads in about 1½ hour.

Macro metrics on 10000 job ads:

| Metric    | Value |
|-----------|-------|
| Accuracy  | 86%   |
| Recall    | 76%   |
| Precision | 81%   |
| F1        | 75%   |

## Background and context

The code in this repository classifies free text from job ads to SSYK4 occupation classification code. A previous approach was [text-2-ssyk][102], which used Random Forests for classification. The approach in this repository, however, uses BERT and seems to yield better results when comparing common classification scores. This repository only contains the code for performing predictions. To train the model, see [`text2ssyk-experiments`][100].

In the bigger picture, this code is part of the [JobLinks Pipeline][103].

## Getting started

Start by [installing it locally](#installing-locally).

Then [run the examples](#running-examples) or [run the tests](#running-tests).

## Installing locally

Make sure you have the following installed:

* Python3
* Poetry
* Pip3
* Git LFS

From the root of the project, call

```
poetry shell
poetry install
```

## Running examples

You can run an example using `make run-example`:

```
jonas@jonas-ostlund-jobtech:~/prog/text2ssyk-bert$ make run-example
echo '[{"originalJobPosting": {"title": "Pizzamästare", "description": "Det är meriterande om du kan baka pizza." }}, {"originalJobPosting": { "description": "Torka damm från datorskärmar och skrivbord.", "title": "Städare"}}]' | make run-stdin
make[1]: Entering directory '/home/jonas/prog/text2ssyk-bert'
poetry run python3 text2ssyk_bert/main.py
{"originalJobPosting": {"title": "Pizzam\u00e4stare", "description": "Det \u00e4r meriterande om du kan baka pizza."}, "ssyk_lvl4": 9411}
{"originalJobPosting": {"description": "Torka damm fr\u00e5n datorsk\u00e4rmar och skrivbord.", "title": "St\u00e4dare"}, "ssyk_lvl4": 9111}
```

## Example with caching

By specifying the `--timeout-seconds` and `--cache-file` options, the results will be cached in a database and the program will only classify jobads until the timeout is reached. Any remaining ads are passed through without being classified. For example, with `--cache-file /tmp/democache.db` and `--timeout-seconds 5`, we will cache the results in the file `/tmp/democache.db` and halt the program after 5 seconds.

Here is an example where we run the program twice and output the SSYK4 class at the `bert-ssyk4` key:
```bash
$ cat text_2_ssyk.out_20k-ads | poetry run python3 text2ssyk_bert/main.py --cache-file /tmp/democache.db --timeout-seconds 5 --class-key bert-ssyk4 > out.jsonl
$ cat out.jsonl | grep bert-ssyk4 | wc -l
13
$ cat text_2_ssyk.out_20k-ads | poetry run python3 text2ssyk_bert/main.py --cache-file /tmp/democache.db --timeout-seconds 5 --class-key bert-ssyk4 > out.jsonl
$ cat out.jsonl | grep bert-ssyk4 | wc -l
31
```

From the output, we see that after the first run, we have classified 13 ads. When we run the program a second time, we use the cache results for the first 12 ads and then have time to classify another 18 ads before the program is halted.

## Running tests

Run the tests with the command

```
make test
```

## Docker

There is a [`Dockerfile`](Dockerfile) for this classifier. 

To build a Docker image, first call
```
make build-docker-image
```

To run an example using that image, call
```
make run-docker-example
```

You will see the following output:
```
echo '{"originalJobPosting": { "description":"baka pizza" }}' | podman run -i text2ssyk
{"originalJobPosting": {"description": "baka pizza"}, "ssyk_lvl4": 9411}
```

## Interactive development

Start by calling `poetry shell` in the terminal.

Then launch the REPL from withing your IDE.

## Retraining the model

The model is trained using a separate repository: [`text2ssyk-experiments`][100]. This is the keep the number of dependencies for this repository low.

Specifically, the code to train the model is found in the file [`bert_classifier_setup.py`][101]. There are Make rules to train the model and export the necessary files.

## License

Copyright 2023 Arbetsförmedlingen JobTech.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental
[101]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental/-/blob/master/text2ssyk_experiments/bert_classifier_setup.py
[102]: https://gitlab.com/arbetsformedlingen/joblinks/text-2-ssyk
[103]: https://gitlab.com/arbetsformedlingen/joblinks/pipeline
