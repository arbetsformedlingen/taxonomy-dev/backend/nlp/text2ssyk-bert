#!/bin/bash
set -euo pipefail
IFS=$'\n\t'


TEXT2SSYK_CONFIG_PATH="${1:-$PWD/data/config0}"
PROJ_DIR="${2:-$PWD}"

export TEXT2SSYK_CONFIG_PATH

apt-get -y update
apt-get -y install sqlite3 jq

echo "INFO: Run 10 ads, creating a cache db" >&2
rm -f democache.db
< tests/testdata/input.10 python3 text2ssyk_bert/main.py --cache-file democache.db --class-key bert-ssyk4 > out_first.jsonl

echo "INFO: Compare with expected" >&2
diff <(jq -S -c . out_first.jsonl) <(jq -S -c . tests/testdata/expected_output.10) || exit 1

echo "INFO: there should now be a democache.db" >&2
if [ ! -f democache.db ]; then
    echo "ERROR: no db file found!" >&2
    exit 1
fi

echo "INFO: We should now have 10 rows in the cache" >&2
if [ $(sqlite3 democache.db "SELECT * FROM Cache;" | wc -l) -ne 10 ]; then
    echo "ERROR: expected 10 rows in the db cache after the first run, but got "$(sqlite3 democache.db "SELECT * FROM Cache;" | wc -l) >&2; exit 1
fi

echo "INFO: Change the cache to only use a fake value" >&2
sqlite3 democache.db "UPDATE Cache SET JobadClass = '011';" >&2

echo "AFTER CHANGE: ">&2
sqlite3 democache.db "SELECT * FROM Cache;"

echo "INFO: Run the 10 ads again, using the cache" >&2
< tests/testdata/input.10 python3 text2ssyk_bert/main.py --cache-file democache.db --class-key bert-ssyk4 >out_second.jsonl

echo "INFO: We should still have 10 rows in the cache" >&2
if [ $(sqlite3 democache.db "SELECT * FROM Cache;" | wc -l) -ne 10 ]; then
    echo "ERROR: expected 10 rows in the db cache after the second run, but got "$(sqlite3 democache.db "SELECT * FROM Cache;" | wc -l) >&2; exit 1
fi

echo "INFO: Prepare an expected result with the fake ssyk value in place" >&2
jq -c '."bert-ssyk4" = "011"' < tests/testdata/expected_output.10 > expected_output_with_testclass.10

echo "INFO: Compare with expected" >&2
diff <(jq -S -c . out_second.jsonl) <(jq -S -c . expected_output_with_testclass.10) || exit 1

cd "$PROJ_DIR"
python3 -m unittest discover
