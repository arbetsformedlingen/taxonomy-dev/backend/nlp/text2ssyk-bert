import unittest
import text2ssyk_bert.bert_classifier as bc

good_jobad = {"headline": "Bagare", "description": {"text": "Bra om du kan baka"}}
bad_jobad = {"headline": 199, "description": {"text": "Bra om du kan baka"}}

class TestBertClassifier(unittest.TestCase):
    def test_text_from_jobad_strategy(self):
        strategy = bc.HeadlineAndDescriptionFromJobad(": ")
        self.assertEqual("Bagare: Bra om du kan baka", strategy(good_jobad))
        self.assertEqual("Bra om du kan baka", strategy(bad_jobad))

        
