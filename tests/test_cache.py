import unittest
from text2ssyk_bert.cache import JobadClassCache, NullJobadClassCache
from pathlib import Path
import tempfile
from datetime import date

def demodate(day):
    return date.fromisoformat("2024-01-{:02d}".format(day))

class TestJobadClassCache(unittest.TestCase):
    def test_null_jobad_class_cache(self):
        with NullJobadClassCache() as cache:
            self.assertIs(None, cache.get_jobad_class("id0"))
            self.assertEqual(0, len(cache))
            cache.visit_jobad(
                "id0",
                date_posted=demodate(9),
                date_valid_until=demodate(29))
            self.assertEqual(0, len(cache))
            self.assertIs(None, cache.get_jobad_class("id0"))
            self.assertEqual(None, cache.get_jobad_data("id0"))
            cache.put_jobad_class("id0", "8908")
            self.assertEqual(None, cache.get_jobad_data("id0"))
            self.assertEqual(None, cache.get_jobad_class("id0"))
            self.assertEqual(0, len(cache))
    
    def test_jobad_class_cache(self):
        with tempfile.TemporaryDirectory() as d:
            db = str(Path(d).joinpath("cache.db"))
            with JobadClassCache(db) as cache:
                self.assertIs(None, cache.get_jobad_class("id0"))
                self.assertEqual(0, len(cache))
                cache.visit_jobad(
                    "id0",
                    date_posted=demodate(9),
                    date_valid_until=demodate(29))
                self.assertEqual(1, len(cache))
                self.assertIs(None, cache.get_jobad_class("id0"))
                self.assertEqual({"jobad_id": "id0",
                                  "jobad_class": None,
                                  "date_posted": demodate(9),
                                  "date_valid_until": demodate(29),
                                  "date_last_visited": None},
                                 cache.get_jobad_data("id0"))
                cache.put_jobad_class("id0", "8908")
                self.assertEqual({"jobad_id": "id0",
                                  "jobad_class": "8908",
                                  "date_posted": demodate(9),
                                  "date_valid_until": demodate(29),
                                  "date_last_visited": None},
                                 cache.get_jobad_data("id0"))
                self.assertEqual("8908", cache.get_jobad_class("id0"))
                self.assertEqual(1, len(cache))
                cache.visit_jobad(
                    "id0",
                    date_valid_until=demodate(30),
                    date_last_visited=demodate(24))
                self.assertEqual({"jobad_id": "id0",
                                  "jobad_class": "8908",
                                  "date_posted": None,
                                  "date_valid_until": demodate(30),
                                  "date_last_visited": demodate(24)},
                                 cache.get_jobad_data("id0"))
                self.assertEqual(1, len(cache))                

                # Calling `put_jobad_class` on a jobad for which we
                # have not first called `visit_jobad` will not do anything
                cache.put_jobad_class("id100000", "1234")
                self.assertEqual(1, len(cache))

                # Test that the previous data is the same.
                self.assertEqual({"jobad_id": "id0",
                                  "jobad_class": "8908",
                                  "date_posted": None,
                                  "date_valid_until": demodate(30),
                                  "date_last_visited": demodate(24)},
                                 cache.get_jobad_data("id0"))

                # Add a new jobad
                cache.visit_jobad(
                    "id1",
                    date_posted=demodate(13),
                    date_last_visited=demodate(14),
                    date_valid_until=demodate(15))
                self.assertEqual(2, len(cache))
                self.assertEqual({"jobad_id": "id1",
                                  "jobad_class": None,
                                  "date_posted": demodate(13),
                                  "date_valid_until": demodate(15),
                                  "date_last_visited": demodate(14)},
                                 cache.get_jobad_data("id1"))
                cache.put_jobad_class("id1", "3333")
                self.assertEqual({"jobad_id": "id1",
                                  "jobad_class": "3333",
                                  "date_posted": demodate(13),
                                  "date_valid_until": demodate(15),
                                  "date_last_visited": demodate(14)},
                                 cache.get_jobad_data("id1"))
                self.assertEqual(2, len(cache))

            # Reopen the database and check that the data is still there.
            with JobadClassCache(db) as cache:
                self.assertEqual({"jobad_id": "id0",
                                  "jobad_class": "8908",
                                  "date_posted": None,
                                  "date_valid_until": demodate(30),
                                  "date_last_visited": demodate(24)},
                                 cache.get_jobad_data("id0"))
                self.assertEqual({"jobad_id": "id1",
                                  "jobad_class": "3333",
                                  "date_posted": demodate(13),
                                  "date_valid_until": demodate(15),
                                  "date_last_visited": demodate(14)},
                                 cache.get_jobad_data("id1"))
                self.assertEqual(2, len(cache))
                
