import unittest
from text2ssyk_bert.cli_argparser import parser

class TestMain(unittest.TestCase):
    def test_parse_empty(self):
        result = parser.parse_args([])
        self.assertIs(None, result.cache_file)
        self.assertEqual("ssyk_lvl4", result.class_key)
        self.assertIs(None, result.timeout_seconds)
        
    def test_parse_cache_file(self):
        result = parser.parse_args(["--cache-file", "/tmp/the-cat.db"])
        self.assertEqual("/tmp/the-cat.db", result.cache_file)
        self.assertEqual("ssyk_lvl4", result.class_key)
        self.assertIs(None, result.timeout_seconds)
        
    def test_parse_cache_file(self):
        result = parser.parse_args(["--class-key", "text2ssyk-resulting-class"])
        self.assertIs(None, result.cache_file)
        self.assertEqual("text2ssyk-resulting-class", result.class_key)
        self.assertIs(None, result.timeout_seconds)
        
    def test_parse_timeout(self):
        result = parser.parse_args(["--timeout-seconds", "119"])
        self.assertIs(None, result.cache_file)
        self.assertEqual("ssyk_lvl4", result.class_key)
        self.assertEqual(119, result.timeout_seconds)
