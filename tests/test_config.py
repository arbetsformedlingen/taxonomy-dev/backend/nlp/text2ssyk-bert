import unittest
from pathlib import Path
import text2ssyk_bert.config as bert_config
from text2ssyk_bert.torch_utils import initialize_torch

cfg = bert_config.ClassifierConfig.from_env()

class TestBertConfig(unittest.TestCase):
    def test_it(self):
        device = initialize_torch(True)
        cl = cfg.make_classifier(device)
        code = cl.classify_jobad({"headline": "Bagare", "description": {"text": "Bra om du kan baka"}})
        self.assertEqual("7612", code)

