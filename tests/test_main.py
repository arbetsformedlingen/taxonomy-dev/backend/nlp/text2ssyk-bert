import unittest
import tempfile
import subprocess
from subprocess import Popen
import json
import text2ssyk_bert.main as main
from datetime import date
from pathlib import Path
from text2ssyk_bert.cache import JobadClassCache

sample_data = {
    "id": '9932xyz',
    "originalJobPosting": {
        "title": 'Pizzamakare sökes till Pizzeria Viking',
        "description": "Våra kunder gillar att äta pizza tills de spricker.",
        "validThrough": '2023-01-10',
        "datePosted": '2022-07-14'
    },

    # This might be the result produced by another classifier
    # earlier in the pipeline.
    "ssyk_lvl4": "2000"
}



def writeln(buf, s):
    buf.write((s + "\n").encode('utf-8'))

def run_popen(args, records):
    base = ['python3',
            'text2ssyk_bert/main.py'];
    base.extend(args)
    with Popen(base,
               stdout=subprocess.PIPE, stdin=subprocess.PIPE) as proc:
        for x in records:
            writeln(proc.stdin, x if isinstance(x, str) else json.dumps(x))
        proc.stdin.close()
        return [json.loads(line.decode('utf-8')) for line in proc.stdout]

class TestMain(unittest.TestCase):
    def test_jobad_properties(self):
        self.assertEqual('9932xyz', main.get_jobad_id(sample_data))
        self.assertEqual(date(2023, 1, 10), main.get_date_valid_until(sample_data))
        self.assertEqual(date(2022, 7, 14), main.get_date_posted(sample_data))

    def test_unreasonable_timeout(self):
        with tempfile.TemporaryDirectory() as d:
            cache_file = Path(d).joinpath("cache.db")
            output = run_popen(['--timeout-seconds', '-100'], # <-- Put a negative timeout here
                               [sample_data])
            self.assertEqual(1, len(output))
            self.assertEqual("2000", output[0]["ssyk_lvl4"])

    def test_with_options(self):
        with tempfile.TemporaryDirectory() as d:
            cache_file = Path(d).joinpath("cache.db")

            # Run at least twice to make sure cache is used.
            for i in range(3):
                output = run_popen(['--timeout-seconds', '60',
                                   '--cache-file', str(cache_file),
                                   '--class-key', 'ssyk-code-in-da-houz'],
                                   [sample_data])
                self.assertEqual(1, len(output))

                result = output[0]
                self.assertTrue(cache_file.exists())
                self.assertEqual("9411", result["ssyk-code-in-da-houz"])

                # This might be the result produced by another classifier
                # earlier in the pipeline.
                self.assertEqual("2000", result["ssyk_lvl4"])

                with JobadClassCache(str(cache_file)) as c:
                    self.assertEqual(1, len(c))
                    self.assertEqual("9411", c.get_jobad_class('9932xyz'))
                    data = c.get_jobad_data('9932xyz')
                    self.assertEqual("9932xyz", data["jobad_id"])
                    self.assertEqual(date(2023, 1, 10), data["date_valid_until"])
                    self.assertEqual(date(2022, 7, 14), data["date_posted"])
                    self.assertEqual("9411", data["jobad_class"])


    def test_full_pipeline(self):
        output = run_popen(
            [],
            [{"originalJobPosting": {"title": "Pizzamästare", "description": "Det är meriterande om du kan baka pizza." }, "id": "abc"},
             {"originalJobPosting": { "description": "Torka damm från datorskärmar och skrivbord.", "title": "Städare"}, "id": "xyz"}])

        expected = [{"originalJobPosting": {"title": "Pizzamästare", "description": "Det är meriterande om du kan baka pizza."}, "id": "abc", 'ssyk_lvl4': "9411" },
                    {"originalJobPosting": { "description": "Torka damm från datorskärmar och skrivbord.", "title": "Städare"}, "id": "xyz", 'ssyk_lvl4': "9111"}]
        self.assertEqual(output, expected)
