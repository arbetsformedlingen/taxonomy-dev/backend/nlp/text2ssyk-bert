class HeadlineAndDescriptionFromJobad:
    def __init__(self, sep):
        self._sep = sep

    def key(self):
        return "headline-and-description"

    def data(self):
        return {"type": self.key(), "sep": self._sep}

    def from_data(self, data):
        return HeadlineAndDescriptionFromJobad(data["sep"])

    def __call__(self, jobad):
        headline = jobad["headline"]
        description = jobad["description"]["text"]
        parts = [s
                 for s in [headline, description]
                 if isinstance(s, str)]
        return self._sep.join(parts)

text_strategy_map = {s.key():s for s in [HeadlineAndDescriptionFromJobad(None)]}

def text_strategy_from_data(data):
    strategy_key = data["type"]
    return text_strategy_map[strategy_key].from_data(data)

def bert_classify(device, tokenizer, bert_model, text, max_token_count):
    return bert_model(
        tokenizer.encode(
            text, return_tensors="pt",
            truncation=True,
            max_length=max_token_count
        ).to(device)).logits.argmax().item()

class BertClassifier:
    def __init__(self, device, label_index_map, tokenizer, max_token_count, bert_model, text_from_jobad_fn):
        self._device = device
        self._label_index_map = label_index_map
        self._index_label_map = {v:k for (k, v) in label_index_map.items()}
        self._tokenizer = tokenizer
        self._max_token_count = max_token_count
        self._bert_model = bert_model
        self._text_from_jobad_fn = text_from_jobad_fn

    def classify_jobad(self, jobad):
        text = self._text_from_jobad_fn(jobad)
        class_index = bert_classify(self._device, self._tokenizer, self._bert_model, text, self._max_token_count)
        return self._index_label_map[class_index]
