import sqlite3 as sql
from datetime import datetime, date
import sys

def _is_optional_instance_of(x, cl):
    return (x is None) or isinstance(x, cl)

def _is_optional_date(x):
    return _is_optional_instance_of(x, date)

def _date_iso_string(x):
    if x is None:
        return None
    else:
        return x.isoformat()

def _parse_date_at_key(dst, k):
    s = dst.get(k)
    if s is not None:
        dst[k] = date.fromisoformat(s)
        
class NullJobadClassCache:
    """This class provides the same interface as JobadClassCache but doesn't store any data. Use it when there is nothing to cache."""
    def __init__(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def visit_jobad(self, jobad_id, date_posted=None, date_valid_until=None, date_last_visited=None):
        pass

    def put_jobad_class(self, jobad_id, jobad_class):
        pass

    def get_jobad_class(self, jobad_id):
        return None

    def get_jobad_data(self, jobad_id):
        return None

    def __len__(self):
        return 0
        
class JobadClassCache:
    def __init__(self, cache_filename):
        assert(isinstance(cache_filename, str))
        self.cache_filename = cache_filename

    def __enter__(self):
        con = sql.connect(self.cache_filename)
        self.con = con
        cur = con.cursor()
        tables = cur.execute("select name from sqlite_master where type='table' and name='Cache'").fetchall()
        if len(tables) == 0:
            print("Creating table Cache.", file=sys.stderr)
            cur.execute("create table Cache (JobadId text not null primary key, JobadClass text, DatePosted text, DateValidUntil text, DateLastVisited text);");
            con.commit()
        else:
            print("Table Cache found.", file=sys.stderr)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.con.close()

    def visit_jobad(self, jobad_id, date_posted=None, date_valid_until=None, date_last_visited=None):
        """Call this method to update the data for a jobad *before* it is processed."""
        assert(isinstance(jobad_id, str))
        assert(_is_optional_date(date_posted))
        assert(_is_optional_date(date_valid_until))
        assert(_is_optional_date(date_last_visited))
        cur = self.con.cursor()
        
        date_posted_s = _date_iso_string(date_posted)
        date_valid_until_s = _date_iso_string(date_valid_until)
        date_last_visited_s = _date_iso_string(date_last_visited)
        
        row = [jobad_id,
               date_posted_s,
               date_valid_until_s,
               date_last_visited_s,
               date_posted_s,
               date_valid_until_s,
               date_last_visited_s]
        cur.execute(
            "insert into Cache(JobadId, DatePosted, DateValidUntil, DateLastVisited) values (?, ?, ?, ?) on conflict (JobadId) do update set DatePosted=?, DateValidUntil=?, DateLastVisited=?",
            row)
        self.con.commit()
        raw_result = self.con.cursor().execute("select * from Cache where JobadId = ?", [jobad_id]).fetchone();

    def _get_jobad_values(self, jobad_id, column_names):
        assert(isinstance(jobad_id, str))
        cur = self.con.cursor()
        column_list = ", ".join(column_names)
        return cur.execute(
            f"select {column_list} from Cache where JobadId = ?",
            [jobad_id]).fetchone();

    def get_jobad_class(self, jobad_id):
        """This method returns the class of a jobad or none."""
        result = self._get_jobad_values(jobad_id, ["JobadClass"])

        if result is None:
            return None
        elif 0 < len(result):
            return result[0]
        else:
            return None

    def get_jobad_data(self, jobad_id):
        raw_result = self.con.cursor().execute("select * from Cache where JobadId = ?", [jobad_id]).fetchone();
        result = self._get_jobad_values(
            jobad_id,
            ["JobadId",
             "JobadClass",
             "DatePosted",
             "DateValidUntil",
             "DateLastVisited"])
        if result is None:
            return None
        data = {k:v
                for (k, v) in zip(
                        ["jobad_id",
                         "jobad_class",
                         "date_posted",
                         "date_valid_until",
                         "date_last_visited"], result)}

        for k in ["date_posted", "date_valid_until", "date_last_visited"]:
            _parse_date_at_key(data, k)
        
        return data

    def __len__(self):
        cur = self.con.cursor()
        return cur.execute("select count(*) from Cache").fetchone()[0]

    def put_jobad_class(self, jobad_id, jobad_class):
        """This method puts sets the class of a jobad previous registered with `visit_jobad`."""
        assert(isinstance(jobad_id, str))
        assert(isinstance(jobad_class, str))
        cur = self.con.cursor()
        cur.execute(
            "update Cache set JobadClass = ? where JobadId = ?",
            [jobad_class, jobad_id])
        self.con.commit()
