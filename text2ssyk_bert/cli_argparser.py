import argparse

parser = argparse.ArgumentParser(prog="text2ssyk-bert", description="Classifies jobads to SSYK4 codes.")

parser.add_argument("--cache-file", type=str, help="Path to cache database file")
parser.add_argument("--class-key", default="ssyk_lvl4", type=str, help="Key at which the class is put in the output map of a jobad")
parser.add_argument("--timeout-seconds", type=int, help="Only classify jobads until this timeout is reached. Any remaining jobads are forwarded without being classified.")

