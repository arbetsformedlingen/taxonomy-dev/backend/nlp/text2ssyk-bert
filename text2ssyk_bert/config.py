import text2ssyk_bert.bert_classifier as bc
from pathlib import Path
from transformers import BertTokenizer, BertForSequenceClassification
import json
import os
import sys

def find_project_root():
    candidate_roots = sys.path
    for candidate_root in candidate_roots:
        root_path = Path(candidate_root)
        if root_path.joinpath("text2ssyk_bert", "main.py").exists():
            return root_path
    raise Error(f"Could not find text2ssyk-bert project root among {candidate_roots}")

def read_main_config_path():
    root = find_project_root()
    with open(root.joinpath("main_config")) as f:
        return root.joinpath(f.read())

def config_path_from_env():
    return Path(os.environ.get("TEXT2SSYK_CONFIG_PATH",
                               read_main_config_path()))

def read_json(filename):
    with open(filename, "r") as f:
        return json.load(f)

class ClassifierConfig:    
    def __init__(self, path):
        self._path = path

    def from_env():
        return ClassifierConfig(Path(config_path_from_env()))

    def label_index_map(self):
        return self._label_index_map

    def num_labels(self):
        return len(self.label_index_map())

    def label_index_map_path(self):
        return self._path.joinpath("label_index_mapping.json")
    
    def trained_bert_path(self):
        return self._path.joinpath("trained_bert")

    def tokenizer_path(self):
        return self._path.joinpath("tokenizer")
    
    def config_path(self):
        return self._path.joinpath("config.json")

    def ssyk4_path(self):
        return self._path.joinpath("ssyk4.json")
    
    def load_trained_bert(self, num_labels):
        return BertForSequenceClassification.from_pretrained(
            str(self.trained_bert_path()),
            #device_map="auto",
            num_labels=num_labels)

    def load_label_index_map(self):
        return read_json(self.label_index_map_path())
    
    def load_config(self):
        return read_json(self.config_path())
    
    def load_tokenizer(self):
        return BertTokenizer.from_pretrained(self.tokenizer_path())

    def load_ssyk4(self):
        return read_json(self.ssyk4_path())

    def ssyk4_map(self):
        return {v["ssyk_code_2012"]:v for v in self.load_ssyk4()}

    def source_url(self):
        return self.load_config()["source_url"]

    def make_classifier(self, device):
        label_index_map = self.load_label_index_map()
        num_labels = len(label_index_map)
        config = self.load_config()
        tokenizer = self.load_tokenizer()
        bert = self.load_trained_bert(num_labels)
        bert.to(device)
        max_token_count = config["max_token_count"]
        text_strategy = bc.text_strategy_from_data(config["text_from_jobad_strategy"])
        return bc.BertClassifier(device, label_index_map, tokenizer, max_token_count, bert, text_strategy)

        

        
if False:
    cfg = ClassifierConfig(Path("/home/jonas/prog/text2ssyk-bert/config0"))
    cl = cfg.make_classifier()
    ssyk4 = cfg.ssyk4_map()
    code = cl.classify_jobad({"headline": "Bagare", "description": {"text": "Bra om du kan baka"}})
    print(f"CODE: {code} which is '{ssyk4[code]['preferred_label']}'")
