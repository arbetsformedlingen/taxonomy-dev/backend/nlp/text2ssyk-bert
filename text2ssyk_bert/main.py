#!/usr/bin/env python3
import os
import subprocess
import sys
from pathlib import Path
import text2ssyk_bert.config as config
import text2ssyk_bert.torch_utils as torch_utils
import json
from text2ssyk_bert.cli_argparser import parser
from datetime import datetime, date
from text2ssyk_bert.cache import JobadClassCache, NullJobadClassCache
from time import time
import urllib.request

def flatten_json_lines(nested_input):
    for line in nested_input:
        queue = [json.loads(line)]
        while 0 < len(queue):
            x = queue[0]
            queue = queue[1:]
            if isinstance(x, list):
                for item in x:
                    queue.append(item)
            else:
                yield x
                
def normalize_jobad(src):
    jobad = src["originalJobPosting"]
    result = {"description": {"text": jobad["description"]},
              "headline": jobad.get("title")}
    return result

def get_jobad_id(src):
    return src["id"]

def parse_date(x):
    return None if x is None else date.fromisoformat(x)

def get_date_posted(src):
    return parse_date(src["originalJobPosting"].get("datePosted"))

def get_date_valid_until(src):
    return parse_date(src["originalJobPosting"].get("validThrough"))

def download_models(cfg):
    for filename in ["optimizer.pt", "pytorch_model.bin"]:
        src_url = f"{cfg.source_url()}/{filename}"
        dst_path = cfg.trained_bert_path().joinpath(filename)
        should_download = not(dst_path.exists())
        action_message = 'DOWNLOAD' if should_download else "SKIP"
        print(f"{action_message} model file at src={src_url} and dst={str(dst_path)}", file=sys.stderr)
        if should_download:
            urllib.request.urlretrieve(src_url, dst_path)

def main():
    args = parser.parse_args()
    prefer_gpu = True
    cfg = config.ClassifierConfig.from_env()
    download_models(cfg)
    device = torch_utils.initialize_torch(prefer_gpu)
    torch_utils.print_info(f"device: {device} of type {type(device)}")
    classifier = cfg.make_classifier(device)
    print(f"cachefile: {args.cache_file}\n", file=sys.stderr)
    cache = NullJobadClassCache() if args.cache_file is None else JobadClassCache(args.cache_file)
    start_time = time()
    with cache:
        for i, original_jobad in enumerate(flatten_json_lines(sys.stdin)):
            current_time = time()
            skip = args.timeout_seconds is not None and start_time + args.timeout_seconds < current_time
            if skip:
                print(json.dumps(original_jobad))
            else:
                date_now = datetime.now().date()
                jobad_id = get_jobad_id(original_jobad)
                cache.visit_jobad(jobad_id,
                                  date_posted=get_date_posted(original_jobad),
                                  date_valid_until=get_date_valid_until(original_jobad),
                                  date_last_visited=date_now)
                jobad = normalize_jobad(original_jobad)
                cached_ssyk_code = cache.get_jobad_class(jobad_id)
                if cached_ssyk_code is None:
                    ssyk_code = classifier.classify_jobad(jobad)
                    cache.put_jobad_class(jobad_id, ssyk_code)
                else:
                    ssyk_code = cached_ssyk_code

                # We output the *original jobad* with the code added to it.
                decorated_jobad = {**original_jobad, args.class_key: str(ssyk_code)}
                print(json.dumps(decorated_jobad, ensure_ascii=False))

if __name__ == "__main__":
    main()
