import torch
import sys

def print_info(s):
    print(s, file=sys.stderr)

def cleanup():
    torch.cuda.empty_cache()

def initialize_torch(prefer_gpu):

    if torch.cuda.is_available():
        print_info("Cuda available!")
    else:
        print_info("No cuda available")

    use_gpu = prefer_gpu and torch.cuda.is_available()
        
    if use_gpu:
        print_info("Use GPU")
        device = torch.device("cuda")
        print_info("Number of devices: " + str(torch.cuda.device_count()))
        for i in range(torch.cuda.device_count()):
            print_info("  * " + torch.cuda.get_device_name(i))
        return device
    else:
        print_info("Use CPU (not the GPU)")
        return torch.device("cpu")
